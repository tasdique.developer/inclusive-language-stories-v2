package entities.servers;

public interface Server {
    @Deprecated
    String getDeprecatedName();

    String getName();
}
