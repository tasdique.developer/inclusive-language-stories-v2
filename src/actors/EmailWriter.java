package actors;

import entities.humans.MalePerson;
import entities.humans.UnidentifiedPerson;
import entities.sayings.Sayings;
import entities.servers.NestedServer;

public class EmailWriter {
    private final StringBuilder message;
    private final UnidentifiedPerson person;
    private final boolean recipientsHistoryIsDefinitelyAmerican;
    private final NestedServer childServer;

    public EmailWriter() {
        message = new StringBuilder();
        person = new UnidentifiedPerson();
        childServer = new NestedServer();
        recipientsHistoryIsDefinitelyAmerican = false;
        buildMessage();
    }

    public String getMessage() {
        return message.toString();
    }

    private void buildMessage() {
        message.append("Hello,");
        endLine();
        endLine();
        message.append("I hope you've been well. We will need two " + person.getPluralPronoun() + " to handle deployments to the " + childServer.getName() + " servers.");
        endLine();
        message.append(Sayings.getSimpleSaying(recipientsHistoryIsDefinitelyAmerican));
        message.append(" So don't stress too much about who it is.");
        endLine();
        endLine();
        message.append("Best,");
        endLine();
        message.append("Dutch Van Der Linde");
    }

    private void endLine() {
        message.append("\n");
    }
}
